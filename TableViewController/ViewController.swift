//
//  ViewController.swift
//  TableViewController
//
//  Created by udnit on 2015/12/1.
//  Copyright © 2015年 udnit. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    let animals = ["Bear","Black Swan","Buffalo","Camel","Cockatoo","Dog","Donkey","Emu","Giraffe","Greater Rhea","Hippopotamis","Horse","Koala","Lion","Llama","Manatus","Meerkat","Panda","Peacook","Pig","Platypus","Polar Bear","Rhinoceros","Seagull","Tasmania Devil","Whale","Whale Shark","Wombat"]
    
    let animalIndexTitles = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    
    var animalsDict = [String: [String]]()
    var animalSectionTitles = [String]()
    var postShown = [Bool](count: 8,repeatedValue:false)
//    var tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createAnimalDict()
        let tableView = UITableView( frame: self.view.bounds, style:UITableViewStyle.Plain)
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(tableView)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createAnimalDict(){
        for animal in animals {
            let animalKey = animal.substringToIndex(animal.startIndex.advancedBy(1))
            if var animalValues = animalsDict[animalKey] {
                animalValues.append(animal)
                animalsDict[animalKey] = animalValues
            }else{
                animalsDict[animalKey] = [animal]
            }
        }
        animalSectionTitles = [String](animalsDict.keys)
        animalSectionTitles.sortInPlace({$0 < $1})
    }
    
    //MARK: UITableViewDataSource
    // INFO HEADER TITLE
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return animalSectionTitles[section]
    }
    // UPDATE HEADER HEIGHT
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    // UPDATE HEADER FONT/COLOR
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.textLabel?.textColor = UIColor.orangeColor()
        headerView.textLabel?.font = UIFont( name: "Avenir", size: 25.0)
    }
    // INFO SECTION COUNT
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return animalSectionTitles.count
    }
    // REQUIRED SECTION ROWS COUNT
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let animalKey = animalSectionTitles[section]
        if let animalValues = animalsDict[animalKey] {
            return animalValues.count
        }
        
        return 0
    }
    // REQUIRED CELL
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) 
        
        let animalKey = animalSectionTitles[indexPath.section]
        if let animalValues = animalsDict[animalKey] {
            cell.textLabel?.text = animalValues[indexPath.row]
        }
        return cell
    }
 
    // INFO SECTION INDEX
    func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
        
        return animalIndexTitles
    }
    // INDEX MAPPING
    func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {
        if let index = animalSectionTitles.indexOf(title) {
            return index
        }
            return -1
    }
    
    
    // ANIMATE
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        //let reuseCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        if postShown[indexPath.row]{
            return
        }
        postShown[indexPath.row] = true
        if indexPath.section % 2 == 1 {
            cell.alpha = 0
            UIView.animateWithDuration(3.0, animations: { cell.alpha = 1 })
        }else{
//            let rotationAngelInRadians = 9.0 * CGFloat(M_PI / 180.0)
//            let rotationTransform = CATransform3DMakeRotation(rotationAngelInRadians, 0, 0, 1)
            let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, -500, 100, 0)
            cell.layer.transform = rotationTransform
            UIView.animateWithDuration(3.0, animations: {cell.layer.transform = CATransform3DIdentity} )
        }
    }

}

